<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8" import="model.User"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
    if(request.getAttribute("user")==null)
	{
		String userName = "";
		String password = "";
		Cookie[] cookies = request.getCookies();
		for(int i = 0; cookies!=null && i < cookies.length; i++)
		{
			if(cookies[i].getName().equals("user"))
			{
				userName = cookies[i].getValue().split("-")[0];
				password = cookies[i].getValue().split("-")[1];
			}
		}
		
		pageContext.setAttribute("user", new User(userName,password));
	}

%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/bootstrap/css/bootstrap-responsive.css" rel="stylesheet">
<script src="${pageContext.request.contextPath}/bootstrap/js/jQuery.js"></script>
<script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.js"></script>
<link href="${pageContext.request.contextPath}/bootstrap/css/application.min.css" rel="stylesheet" />
<title>登陆</title>
<style type="text/css">
	  body {
        background-image: url('images/backImage.jpg');
      }
      
      .form-signin-heading{
      	text-align: center;
      }

      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }
	.navcss{
		max-width: 500px;
        padding: 19px 29px 0px;
        margin: 200px auto auto 400px;
	}
</style>
<script type="text/javascript">
	function checkForm(){
		var userName=document.getElementById("userName").value;
		var password=document.getElementById("password").value;
		if(userName==null || userName==""){
			document.getElementById("error").innerHTML="用户名不能为空";
			return false;
		}
		if(password==null || password==""){
			document.getElementById("error").innerHTML="密码不能为空";
			return false;
		}
		return true;
	}
</script>
</head>
<body>

	<div class="container">
		<nav id="sidebar" class="sidebar nav-collapse collapse navcss" style="width: 310px;">
		      <form name="myForm" class="form-signin" action="login" method="post" onsubmit="return checkForm()">
		        <h2 class="form-signin-heading">LV.5日记本</h2>
		        <input id="userName" name="userName" value="${user.userName }"  type="text" class="input-block-level" placeholder="用户名..." style="width: 300px;">
		        <input id="password" name="password" value="${user.password }"   type="password" class="input-block-level" placeholder="密码..." style="width: 300px;">
		        <label class="checkbox">
		          <input id="remember" name="remember" type="checkbox" value="remember-me">记住我 &nbsp;&nbsp;&nbsp;&nbsp; <font id="error" color="red">${error }</font>  
		        </label>
		        <button class="btn btn-default btn-success" type="submit">登录</button>
		        &nbsp;&nbsp;&nbsp;&nbsp;
		        <button class="btn btn-default btn-success" type="button" >重置</button>
		
				<p align="center" style="padding-top: 15px;">版权所有  2014  百度  <a href="http://www.baidu.com" target="_blank">www.baidu.com</a></p>
		      </form>
		</nav>
	</div>

</body>
</html>